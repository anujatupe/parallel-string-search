#include <string.h>
#include <stdio.h>
#include <pthread.h>
/*
 * Finds the number of lines present in the file
 */

int no_of_lines_per_thread;
char *pattern;
//FILE *thread_file_ptrs[];

int get_number_of_lines(char *filename) {
	FILE *fp = fopen(filename,"r");
	char ch = 0;
	int lines = 0;
	while(!feof(fp)) {
	  ch = fgetc(fp);
	  if(ch == '\n') {
	    lines++;
	  }
	}
	printf("The number of lines are: %d\n", lines);
	fclose(fp);
	return lines;
}

void* read_file_contents(void* thread_number) {
	int my_thread_number = (int)thread_number;
	//printf("\nThis is thread %d, %c", my_thread_number, fgetc(thread_file_ptrs[my_thread_number]));
	return NULL;
}

// int main(int argc, char *argv[]) {
// 	int i = 0;
// 	int no_lines = get_number_of_lines(argv[1]);
// 	//int no_threads = (int) argv[2];
// 	int no_threads = 4;
// 	pthread_t* thread_handles;

// 	thread_handles = malloc(no_threads*sizeof(pthread_t));

// 	no_of_lines_per_thread = no_lines/no_threads;
// 	FILE *fp = fopen(argv[1],"r");
	
// 	thread_file_ptrs[0] = fopen(argv[1],"r");

// 	pthread_create(&thread_handles[0], NULL, read_file_contents, (void*) i);
// 	for(i = 1; i <= (no_threads - 1); i++) {
// 		char ch = 0;
// 		int lines = 0;
// 		while(!feof(fp) && lines <= i*no_of_lines_per_thread) {
// 		  ch = fgetc(fp);
// 		  if(ch == '\n') {
// 		    lines++;
// 		  }
// 		}
// 		thread_file_ptrs[i] = fdopen (dup (fileno (fp)), "r");
// 		pthread_create(&thread_handles[i], NULL, read_file_contents, (void*) i);
// 	}

// 	for(i = 0; i < no_threads; i++) {
// 			pthread_join(thread_handles[i], NULL);
// 	}

// 	free(thread_handles);

// 	return 0;
// }

int main(int argc, char *argv[]) {
	size_t len = 0;
    //ssize_t read;
    int i = 0;
	int no_lines = get_number_of_lines(argv[1]);
	//int no_threads = (int) argv[2];
	char *text_lines[no_lines];
	int no_threads = 4;
	//pthread_t* thread_handles;

	//thread_handles = malloc(no_threads*sizeof(pthread_t));

	no_of_lines_per_thread = no_lines/no_threads;
	FILE *fp = fopen(argv[1],"r");

	
	while ((i < no_lines) && (getline(&text_lines[i], &len, fp) != -1)) {
           //printf("Retrieved line of length %zu :\n", read);
           printf("Line %d: %s", i, text_lines[i]);
           i++;
           if (i < no_lines) {
           	text_lines[i] = NULL;	
           }
           
    }
	
    return 0;

}