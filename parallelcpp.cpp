#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <pthread.h>
#include <string>
#include <cstring>
#include <time.h>
#include <fstream>
#include <cstdint>

#define SECINNANOSEC 1000000000L

using namespace std;

long no_threads;
long no_lines;
long no_of_lines_per_thread;
long left_over_lines;
char pattern[150];
vector<string> text;
pthread_mutex_t occurrences_lock;
int total_occurences = 0;


/*
 * Finds the number of lines present in the file
 */
int get_number_of_lines(char *filename) {
  FILE *fp = fopen(filename,"r");
  char ch = 0;
  int lines = 0;
  while(!feof(fp)) {
    ch = fgetc(fp);
    if(ch == '\n') {
      lines++;
    }
  }
  //printf("The number of lines are: %d\n", lines);
  fclose(fp);
  return lines;
}

void* find_pattern_in_text(void* thread_id) {
  
  
  long line_count = 0;
  long my_thread_id = (long) thread_id;
  long no_lines_limit;
  long no_lines_for_every_thread = 0;
  no_lines_limit = no_of_lines_per_thread;
  int occurrences = 0;
  int occurence_on_every_line = 0;
  int is_found = 0;
  int start;
  int line_count_start_value = 0;

  if (my_thread_id == (no_threads - 1)) {
    no_lines_limit = no_of_lines_per_thread + left_over_lines;
    no_lines_for_every_thread = ((my_thread_id+1)*no_of_lines_per_thread) + left_over_lines;
  } else {
    no_lines_for_every_thread = ((my_thread_id+1)*no_of_lines_per_thread);
  }
	
  line_count_start_value = (my_thread_id*no_lines_limit);
  
  for (line_count = line_count_start_value; line_count < no_lines_for_every_thread; line_count++){
    string::size_type start = 0;
    is_found = 0;
    occurence_on_every_line = 0;

    while ((start = text[line_count].find(pattern, start)) != string::npos) {
      is_found = 1;
      ++occurrences;
      ++occurence_on_every_line;
      start += strlen(pattern);
    }

    if (is_found == 1) {
      std::cout << "\n Thread "<<my_thread_id<<" - Found "<<occurence_on_every_line<<" occurences at Line " <<(line_count+1);
    }
  }
  

  pthread_mutex_lock(&occurrences_lock);
  total_occurences += occurrences;
  pthread_mutex_unlock(&occurrences_lock);

  cout<<"\n*** Number of occurrences in thread "<<my_thread_id<<" are "<<occurrences<<" ***\n";
  return NULL;
}

int main(int argc, char *argv[]) {
  timespec start_time, end_time;
  uint64_t diff_time;
  size_t len = 0;
  long i = 0;
  no_lines = get_number_of_lines(argv[1]);
  string current_line;
  no_threads = atoi(argv[2]);

  strcpy(pattern, argv[3]);
	
  pthread_t* thread_handles;
	

  thread_handles = static_cast<pthread_t*>(malloc(no_threads*sizeof(pthread_t)));

  no_of_lines_per_thread = no_lines/no_threads;
  left_over_lines = no_lines % no_threads;

  ifstream filename(argv[1]);

  while(getline(filename, current_line)) {
    text.push_back(current_line);
    i++;
  }

  clock_gettime(CLOCK_MONOTONIC, &start_time);
  for (i = 0; i < no_threads; i++) {
    pthread_create(&thread_handles[i], NULL, find_pattern_in_text, (void*) i);
  }

  for (i = 0; i < no_threads; i++) {
    pthread_join(thread_handles[i], NULL);
  }
  clock_gettime(CLOCK_MONOTONIC, &end_time);
  
  diff_time = SECINNANOSEC * (end_time.tv_sec - start_time.tv_sec) + end_time.tv_nsec - start_time.tv_nsec;

  cout<<"\n\n*** Time required to execute parallel version of the program: "<<(long long unsigned int) diff_time<<" nanoseconds ***\n\n";

  pthread_mutex_lock(&occurrences_lock);
  cout<<"\n---------------------------------------------------------------------------------";
  cout<<"\n************** TOTAL NO OF OCCURENCES IN FILE ARE "<<total_occurences<<" ****************\n\n";
  pthread_mutex_unlock(&occurrences_lock);
  

  free(thread_handles);

  return 0;
}