#include <stdio.h>
#include <stdlib.h>
#include <iostream>
#include <vector>
#include <string>
#include <cstring>
#include <time.h>
#include <fstream>
#include <cstdint>

#define SECINNANOSEC 1000000000L

using namespace std;

long no_lines;
char pattern[150];
vector<string> text;

timespec time_taken_for_search(timespec start_time, timespec end_time)
{
  timespec diff_time;
  if ((end_time.tv_nsec - start_time.tv_nsec) < 0) {
    diff_time.tv_sec = end_time.tv_sec - start_time.tv_sec - 1;
    diff_time.tv_nsec = 1000000000 + end_time.tv_nsec - start_time.tv_nsec;
  } else {
    diff_time.tv_sec = end_time.tv_sec - start_time.tv_sec;
    diff_time.tv_nsec = end_time.tv_nsec - start_time.tv_nsec;
  }
  return diff_time;
}

/*
 * Finds the number of lines present in the file
 */
int get_number_of_lines(char *filename) {
  FILE *fp = fopen(filename,"r");
  char ch = 0;
  int lines = 0;
  while(!feof(fp)) {
    ch = fgetc(fp);
    if(ch == '\n') {
      lines++;
    }
  }
  //printf("The number of lines are: %d\n", lines);
  fclose(fp);
  return lines;
}

void* find_pattern_in_text() {
  
  long line_count = 0;
  int occurrences = 0;
  int occurence_on_every_line = 0;
  int is_found = 0;
  int start;
	
  for (line_count = 0; line_count < no_lines; line_count++){
    string::size_type start = 0;
    is_found = 0;
    occurence_on_every_line = 0;

    while ((start = text[line_count].find(pattern, start)) != string::npos) {
      is_found = 1;
      ++occurrences;
      ++occurence_on_every_line;
      start += strlen(pattern);
    }

    if (is_found == 1) {
      std::cout << "\nFound "<<occurence_on_every_line<<" occurences at Line " <<(line_count+1);
    }
  }
  
  cout<<"\n---------------------------------------------------------------------------------";
  cout<<"\n************** TOTAL NO OF OCCURENCES IN FILE ARE "<<occurrences<<" ******************\n\n";
  return 0;
}

int main(int argc, char *argv[]) {
  size_t len = 0;
  long i = 0;
  string current_line;
  no_lines = get_number_of_lines(argv[1]);
  timespec start_time, end_time;
  uint64_t diff_time;
  char *text_lines[no_lines];

  strcpy(pattern, argv[2]);

  ifstream filename(argv[1]);

  while(getline(filename, current_line)) {
    text.push_back(current_line);
    i++;
  }

  clock_gettime(CLOCK_MONOTONIC, &start_time);
  find_pattern_in_text();
  clock_gettime(CLOCK_MONOTONIC, &end_time);

  diff_time = SECINNANOSEC * (end_time.tv_sec - start_time.tv_sec) + (end_time.tv_nsec - start_time.tv_nsec);

  cout<<"\n\n*** Time required to execute sequential version of the program: "<<(long long unsigned int) diff_time<<" nanoseconds ***\n\n";

  return 0;
}